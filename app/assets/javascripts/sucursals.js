function SucursalView() {
    $('form').submit(function(e) { 
        e.preventDefault();
        
        var _this = $(this); //store form so it can be accessed later
        var errors = 0;
        var msgs="";

        //Validar que exista nombre
        if( !$("#sucursal_nombre").val() ) {
            
            $("#sucursal_nombre").addClass("red-border");
            errors++;
            msgs+= "Sucursal debe tener nombre \n";     
        }else{
            $("#sucursal_nombre").removeClass("red-border");
        }


        if( $("#sucursal_numeroExt").val() ) {
            //Validar que sea numero
            if (!$.isNumeric($("#sucursal_numeroExt").val())){
            $("#sucursal_numeroExt").addClass("red-border");
            errors++;
            msgs+= "Numero exterior debe ser un numero \n";
            }
            
        }else{
            $("#sucursal_numeroExt").removeClass("red-border");
        }

        if( $("#sucursal_numero_int").val() ) {
            //Validar que sea numero
            if (!$.isNumeric($("#sucursal_numero_int").val())){
            $("#sucursal_numero_int").addClass("red-border");
            errors++;
            msgs+= "Numero interior debe ser un numero \n";
            }else{
                $("#sucursal_numero_int").removeClass("red-border");
            }
            
        }else{
            $("#sucursal_numero_int").removeClass("red-border");
        }

        if( $("#sucursal_codigoPostal").val() ) {
            //Validar que sea numero
            if (!$.isNumeric($("#sucursal_codigoPostal").val())){
            $("#sucursal_codigoPostal").addClass("red-border");
            errors++;
            msgs+= "Codigo postal debe ser un numero \n";
            }else {
                if ($("#sucursal_codigoPostal").val().length != 5){
                    errors++;
                    msgs+= "Codigo postal debe ser de 5 digitos \n";
                    
                }else{
                    $("#sucursal_codigoPostal").removeClass("red-border");
                }
            }
            
        }else{
            $("#sucursal_codigoPostal").removeClass("red-border");
        }

 
        if(errors > 0){
            alert(msgs);
            return false;
        }
        _this.unbind('submit').submit();
    });
}
