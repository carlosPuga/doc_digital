function EmpleadosView() {
    $("form").validate({
        //error place
        errorPlacement: function (error, element) {
          error.insertBefore(element);
        },
      //adding rule
        rules: {
        // username is required with max of 20 and min of 6
        "empleado[nombre]":{
          required: true,
        },
        "empleado[rfc]":{
            required: true,
          },
        },
        // error messages
        messages: {
          "empleado[nombre]":{
            required: "Nombre es requerido."
          },
          "empleado[rfc]":{
            required: "RFC es requerido."
          },
        }
      });
 
}


//_this.unbind('submit').submit();
/*

    $('form').submit(function(e) { 
        e.preventDefault();
        
        var _this = $(this); //store form so it can be accessed later
        var errors = 0;

        $("form :input").map(function(){
            if( !$(this).val() ) {
                $(this).addClass("red-border");
                errors++;     
            }
        });
        if(errors > 0){
            alert("Todos los campos son requeridos");
            return false;
        }
        _this.unbind('submit').submit();
    });
*/