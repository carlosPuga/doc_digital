function RegistrationView() {
    $("form").validate({
        //error place
        errorPlacement: function (error, element) {
          error.insertBefore(element);
        },
      //adding rule
        rules: {
        // username is required with max of 20 and min of 6
        "user[name]":{
          required: true,
        },
        "user[rfc]":{
            required: true,
            maxlength: 13,
            minlength: 12,
            remote: {
              url: "/empresas/validar_rfc",
              type: "post",
              data: {
                rfc: function() {
                  return $("user[rfc]").val();
                }
              }
            }
          },
        "user[empresa]":{
          required: true,
        },
        "user[email]":{
            required: true,
          },
        "user[password]":{
          required: true,
        },
        "user[password_confirmation]":{
            required: true,
          }
        },
        // error messages
        messages: {
          "user[name]":{
            required: "Nombre es requerido."
          },
          "user[rfc]":{
            required: "RFC es requerido.",
            remote: "RFC ya ha sido usado"
          },
          "user[empresa]":{
            required: "Empresa es requerido."
          },
          "user[email]":{
            required: "Email es requerido."
          },
          "user[password]":{
            required: "Password es requerido."
          },
          "user[password_confirmation]":{
            required: "Password confirmation es requerido."
          },
        }
      });
 
}
