class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  belongs_to :empresa, required: false
  
  def self.validar_rfc(rfc)
    u = User.find_by(rfc: rfc)
    if u.present?
      return false
    end
    return true
    
  end

end
