class EmpresasController < ApplicationController
    protect_from_forgery :except => [:validar_rfc]
    skip_before_action :authenticate_user!, only: [:validar_rfc]

    def validar_rfc
        rfc_encontrado = User.find_by(rfc: params[:user][:rfc])
        message = rfc_encontrado ? "ERROR" : "SUCCESS"
        if message == "SUCCESS"
            puts("true")
            render :json => true
            #render json: {response: "true"}, status: 200
        else
            puts("false")
            render :json => false
        end
    end


end
