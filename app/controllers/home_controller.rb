class HomeController < ApplicationController
    def index
        @sucursales = current_user.empresa.sucursals
    end
end
