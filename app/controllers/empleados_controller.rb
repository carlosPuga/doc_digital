class EmpleadosController < ApplicationController
    before_action :set_empleado, only: [:show, :edit, :update]

    def index
        @empleados = Empleado.all.where(sucursal_id: params[:sucursal_id])
    end
    def show
        
    end

    def edit

    end

    def update
    respond_to do |format|
        if @empleado.update(empleado_params)
            format.html { redirect_to root_path, notice: "empleado fue actualizado con éxito." }
            format.json { render :show, status: :ok, location: @empleado }
        else
            format.html { render :edit }
            format.json { render json: @empleado.errors, status: :unprocessable_entity }
        end
        end
    end

    def new
        @empleado = Empleado.new
    end

    def create
        @empleado = Empleado.new(empleado_params)
        respond_to do |format|
            if @empleado.save
                format.html { redirect_to @empleado, notice: 'empleado fue creado con éxito.' }
                format.json { render :show, status: :created, location: @empleado }
            else
                format.html { render :new }
                format.json { render json: @empleado.errors, status: :unprocessable_entity }
            end
        end
    end
    
    private
    def set_empleado
        @empleado = Empleado.find(params[:id])
    end
    def empleado_params
        params.require(:empleado).permit(:nombre, :rfc, :puesto, :sucursal_id)
    end

end
