class SucursalsController < ApplicationController
    before_action :set_sucursal, only: [:show, :edit, :update]

    def index
    end
    def show
        
    end

    def edit
        @empleados = @sucursal.empleados
    end

    def update
    respond_to do |format|
        if @sucursal.update(sucursal_params)
            format.html { redirect_to root_path, notice: "sucursal fue actualizado con éxito." }
            format.json { render :show, status: :ok, location: @sucursal }
        else
            format.html { render :edit }
            format.json { render json: @sucursal.errors, status: :unprocessable_entity }
        end
        end
    end

    def new
        @sucursal = Sucursal.new
    end

    def create
        @sucursal = Sucursal.new(sucursal_params)
        @sucursal.empresa_id = current_user.empresa_id
        respond_to do |format|
            if @sucursal.save
                format.html { redirect_to @sucursal, notice: 'Sucursal fue creado con éxito.' }
                format.json { render :show, status: :created, location: @sucursal }
            else
                format.html { render :new }
                format.json { render json: @sucursal.errors, status: :unprocessable_entity }
            end
        end
    end
    
    private
    def set_sucursal
        @sucursal = Sucursal.find(params[:id])
    end

    def sucursal_params
        params.require(:sucursal).permit(:nombre, :calle, :colonia, :numeroExt, :numero_int, :codigoPostal, :ciudad, :pais, :empresa_id)
    end


end
