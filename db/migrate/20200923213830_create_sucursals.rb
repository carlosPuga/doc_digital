class CreateSucursals < ActiveRecord::Migration[5.2]
  def change
    create_table :sucursals do |t|
      t.string :nombre
      t.string :calle
      t.string :colonia
      t.integer :numeroExt
      t.integer :numero_int
      t.integer :codigoPostal
      t.string :ciudad
      t.string :pais

      t.timestamps
    end
  end
end
