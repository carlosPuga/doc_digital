class AddRfcToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :rfc, :string
  end
end
