class AddEmpresaToSucursals < ActiveRecord::Migration[5.2]
  def change
    add_reference :sucursals, :empresa, foreign_key: true
  end
end
